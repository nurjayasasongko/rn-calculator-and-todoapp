import React from 'react';
import { StyleSheet, Text, View, ImageBackground, TextInput, TouchableOpacity } from 'react-native';
import { AntDesign, FontAwesome5 } from '@expo/vector-icons';

export default function Header(props) {
  return (
    <ImageBackground resizeMode="cover" style={styles.container}>
      <View style={styles.wrapper}>
        <View style={styles.row}>
          <Text style={styles.textTitle}>Todo List</Text>
          <TouchableOpacity onPress={props.onShowPress}>
            <FontAwesome5 name="edit" size={20} color="hsl(234, 39%, 85%)" />
          </TouchableOpacity>
        </View>

        <View style={{ position: 'relative' }}>
          <TextInput style={styles.input} placeholder="Create a New Task" placeholderTextColor="hsl(234, 11%, 52%)" onChangeText={props.onChangeText} value={props.value} />
          <TouchableOpacity style={styles.icon} onPress={props.onPress}>
            <AntDesign name="plus" size={20} color="#FFFFFF" />
          </TouchableOpacity>
        </View>
      </View>
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: 180,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    backgroundColor: '#26292B',
  },
  wrapper: {
    width: '100%',
    paddingHorizontal: 30,
    paddingVertical: 60,
  },
  textTitle: {
    color: 'white',
    fontSize: 30,
  },
  input: {
    position: 'relative',
    color: '#2E3239',
    fontSize: 17,
    width: '100%',
    height: 40,
    marginTop: 20,
    paddingHorizontal: 10,
    backgroundColor: '#FFFFFF',
    borderRadius: 10,
    shadowColor: 'black',
    shadowOffset: { width: 2, height: 10 },
    shadowRadius: 10,
    elevation: 20,
  },
  icon: {
    position: 'absolute',
    padding: 2,
    top: 25,
    right: 10,
    elevation: 23,
    backgroundColor: '#26292B',
    borderWidth: 1,
    borderColor: 'hsl(233, 14%, 35%)',
    borderRadius: 50,
  },
  placeholder: {
    color: 'white',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});
